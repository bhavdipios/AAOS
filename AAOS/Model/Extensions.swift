//
//  Extensions.swift
//  Darabeel
//
//  Created by technosoft on 25/12/17.
//  Copyright © 2017 technosoft. All rights reserved.
//

import UIKit


public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}

extension UIPageControl {
    
    func customPageControl(dotFillColor:UIColor, dotBorderColor:UIColor, dotBorderWidth:CGFloat) {
        for (pageIndex, dotView) in self.subviews.enumerated() {
            if self.currentPage == pageIndex {
                dotView.backgroundColor = dotFillColor
                dotView.layer.cornerRadius = dotView.frame.size.height / 2
            }else{
                dotView.backgroundColor = .clear
                dotView.layer.cornerRadius = dotView.frame.size.height / 2
                dotView.layer.borderColor = dotBorderColor.cgColor
                dotView.layer.borderWidth = dotBorderWidth
            }
        }
    }
    
}

extension UIViewController {
    
    func presentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            print("You've pressed OK Button")
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
extension UIImage {
    
    /// Returns a image that fills in newSize
    func resizedImage(newSize: CGSize) -> UIImage {
        // Guard newSize is different
        guard self.size != newSize else { return self }
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        self.draw(in: CGRect(x: 0,y: 0,width: newSize.width,height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    /// Returns a resized image that fits in rectSize, keeping it's aspect ratio
    /// Note that the new image size is not rectSize, but within it.
    func resizedImageWithinRect(rectSize: CGSize) -> UIImage {
        let widthFactor = size.width / rectSize.width
        let heightFactor = size.height / rectSize.height
        
        var resizeFactor = widthFactor
        if size.height > size.width {
            resizeFactor = heightFactor
        }
        
        let newSize = CGSize(width: size.width/resizeFactor,height: size.height/resizeFactor)
        let resized = resizedImage(newSize: newSize)
        return resized
    }
    
}
extension UITextField{
    
        func addBottomLine(_ color: UIColor) {
            let border = CALayer()
            let width = CGFloat(1.0)
            border.borderColor = color.cgColor
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width+30, height: self.frame.size.height)
            
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
        }
    
    func validatePhoneNumber(value: String) -> Bool {
        
        let PHONE_REGEX = "^[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    func RemoveWhiteSpace() {
        self.text = self.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
        
    }
    
//    func addBottomLine() {
//        let border = CALayer()
//        let width = CGFloat(1.0)
//        border.borderColor = UIColor.lightGray.cgColor
//        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
//        
//        border.borderWidth = width
//        self.layer.addSublayer(border)
//        self.layer.masksToBounds = true
//    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Helvetica Neue", size: 12)!]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}

extension UITextView {
    func simple_scrollToBottom() {
        let textCount: Int = text.count
        guard textCount >= 1 else { return }
        scrollRangeToVisible(NSMakeRange(textCount - 1, 1))
    }
}

extension UILabel {
    
    func addShadow() {
        
        
        self.clipsToBounds = true
        self.layer.masksToBounds = false
        let shadowSize : CGFloat = 5.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: self.frame.size.width + shadowSize,
                                                   height: self.frame.size.height + shadowSize))
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.cornerRadius = 8
        
        self.layer.masksToBounds = true
    }
    
}
extension UIButton {
    
    func addShadow() {
        self.layer.cornerRadius = 8
        self.clipsToBounds = true
        
        let shadowSize : CGFloat = 5.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: self.frame.size.width + shadowSize,
                                                   height: self.frame.size.height + shadowSize))
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    func applyGradientButtonBorder() {
        
        let color1 = UIColor(red: 37.0/255.0, green: 152.0/255.0, blue: 77.0/255.0, alpha: 1.0)
        let color2 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        let color3 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradient.colors = [color1,color3,color2]
        
        let shape = CAShapeLayer()
        shape.lineWidth = 1.5
        shape.path = UIBezierPath(rect: self.bounds).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        
        self.layer.addSublayer(gradient)
        
       
    }
    
    func applyGradientButton() {
        
        let color1 = UIColor(red: 37.0/255.0, green: 152.0/255.0, blue: 77.0/255.0, alpha: 1.0)
        let color2 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        let color3 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        
        let gradientLayer = CAGradientLayer()
      //  gradientLayer.frame = self.bounds
        gradientLayer.colors = [color3.cgColor, color2.cgColor, color1.cgColor]
        
      
        gradientLayer.frame = self.bounds
        gradientLayer.cornerRadius = 8
        //self.layer.addSublayer(gradientLayer)
        
       // let col = gradientLayer.backgroundColor as cgColor?
        
       self.backgroundColor = UIColor(patternImage: self.image(fromLayer: gradientLayer))
    }
    
    func applyGradient(colours: [UIColor]) -> Void {
        
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.cornerRadius = 8
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage!
    }
}

extension UIImage {
    
    func GradientImage() -> UIImage {
        
        let color1 = UIColor(red: 37.0/255.0, green: 152.0/255.0, blue: 77.0/255.0, alpha: 1.0)
        let color2 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        let color3 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        let gradientLayer = CAGradientLayer()
        // gradientLayer.frame =  UINavigationBar.appearance().bounds
        gradientLayer.colors = [color3.cgColor, color2.cgColor, color1.cgColor]
        
        let sizeLength = UIScreen.main.bounds.size.height * 2
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 64)
        gradientLayer.frame = defaultNavigationBarFrame
        // btn_SignIn.setBackgroundImage(self.image(fromLayer: gradientLayer), for: .default)
        
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage!
        
    }
    
    
   
}

extension UINavigationController {
    
    func applyGradientNavi() {
        
        let color1 = UIColor(red: 37.0/255.0, green: 152.0/255.0, blue: 77.0/255.0, alpha: 1.0)
        let color2 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        let color3 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        let gradientLayer = CAGradientLayer()
        // gradientLayer.frame =  UINavigationBar.appearance().bounds
        gradientLayer.colors = [color3.cgColor, color2.cgColor, color1.cgColor]
        
        let sizeLength = UIScreen.main.bounds.size.height * 2
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 64)
        gradientLayer.frame = defaultNavigationBarFrame
       // btn_SignIn.setBackgroundImage(self.image(fromLayer: gradientLayer), for: .default)
        
        self.navigationController?.navigationBar.setBackgroundImage(self.image(fromLayer: gradientLayer), for: .default)
    }
    
    
    func image(fromLayer layer: CAGradientLayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage!
    }
}

extension UINavigationBar {
    
    func applyGradientNaviBar() {
        
        
        let color1 = UIColor(red: 37.0/255.0, green: 152.0/255.0, blue: 77.0/255.0, alpha: 1.0)
        let color2 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        let color3 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        let gradientLayer = CAGradientLayer()
        // gradientLayer.frame =  UINavigationBar.appearance().bounds
        gradientLayer.colors = [color3.cgColor, color2.cgColor, color1.cgColor]
        
        let sizeLength = UIScreen.main.bounds.size.height * 2
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 64)
        gradientLayer.frame = defaultNavigationBarFrame
        // btn_SignIn.setBackgroundImage(self.image(fromLayer: gradientLayer), for: .default)
        
        
       // self.setBackgroundImage(<#T##backgroundImage: UIImage?##UIImage?#>, for: <#T##UIBarMetrics#>)
        self.setBackgroundImage(self.image(fromLayer: gradientLayer), for: .default)
    }
    func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage!
    }
}

extension UITextField{
    
    func placeholdercolor(_ color: UIColor) {
        
        var placeholderText = ""
        if self.placeholder != nil{
            placeholderText = self.placeholder!
        }
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedStringKey.foregroundColor : color])
        
        self.textColor = color
    }
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.clear.cgColor
        
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
   

    
    func setValidationMsg(_ strMsg: String) {
        
        
   
        // CGRectMake has been deprecated - and should be let, not var
        let label = UILabel(frame: CGRect(x: 0, y: self.frame.size.height, width: self.frame.size.width, height: 21))
        
        // you will probably want to set the font (remember to use Dynamic Type!)
        label.font = UIFont.preferredFont(forTextStyle: .footnote)
        
        // and set the text color too - remember good contrast
        label.textColor = .red
        
        // may not be necessary (e.g., if the width & height match the superview)
        // if you do need to center, CGPointMake has been deprecated, so use this
       // label.center = CGPoint(x: 160, y: 284)
        
        // this changed in Swift 3 (much better, no?)
        label.textAlignment = .right
        
        label.text = strMsg
        
       self.addSubview(label)
        
    }
    
    
}
extension UIView {
    
    
    func roundCornersVw(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func setShadow(_ BKcolor: UIColor) {
        
        self.backgroundColor = BKcolor
        self.layer.shadowColor =  ConstantsModel.ColorCode.kColor_Theme.cgColor
        self.layer.shadowOffset = CGSize(width: -1, height: 2.0)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 5.0
        self.layer.cornerRadius = 8.0
        self.clipsToBounds = true
        
    }

    
    func addShadowOnView() {
      //  self.layer.cornerRadius = 8
      //  self.clipsToBounds = true
        
//        let shadowSize : CGFloat = 9.0
//        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
//                                                   y: -shadowSize / 2,
//                                                   width: self.frame.size.width + shadowSize,
//                                                   height: self.frame.size.height + shadowSize))
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 5.0, height: 7.0)
        self.layer.shadowOpacity = 0.1
//        self.layer.shadowPath = shadowPath.cgPath
        self.layer.shadowRadius = 0.8
      //  view.layer.shadowOpacity = 1
    }
    
    
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
            self.alpha = 0.0
            
            UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.isHidden = false
                self.alpha = 1.0
            }, completion: completion)
    }
    
    
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        
            self.alpha = 1.0
            
            UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.alpha = 0.0
            }) { (completed) in
                self.isHidden = true
                completion(true)
            }
    }
    
    
    func applyGradientView() {
        
          
        
    }
    
}
extension UIButton{
    
    
    func roundCornersBtn(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    
    func setThemePattern(_ BKcolor: UIColor,_ TXcolor: UIColor) {
        
        self.layer.cornerRadius = 8
        self.backgroundColor = BKcolor
        self.setTitleColor(TXcolor , for: .normal)
        self.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 16)!
        self.layer.borderColor = TXcolor.cgColor
        self.layer.borderWidth = 0.8
        
    }
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
    
    
}


extension String {
    
    
    
    func DetectCategoryText(selectedCategoryId: Int) -> String {
        
        
        var catName: String!

        if selectedCategoryId == 1 {
            
            catName = "-Document"
            
        } else if selectedCategoryId == 2 {
            
            
            catName = "-Business Card"
            
        } else if selectedCategoryId == 3 {
            
            catName = "-Barcode-QR-Code"
            
            
        } else if selectedCategoryId == 4 {
            
            catName = "-Signature"
            
            
        }else if selectedCategoryId == 5 {
            
            catName = "-Credit Card"
            
        }else if selectedCategoryId == 6 {
            
            catName = "-Adhar-Card"
            
            
            
        }else if selectedCategoryId == 7 {
            
            catName = "-Pancard"
            
            
        }else if selectedCategoryId == 8 {
            
            
            catName = "-passport"
            
            
        }else if selectedCategoryId == 9 {
            
            catName = "- driving licenc"
          
            
        }
        
        return catName
        
    }
    
    
}


extension String {
    func sha1() -> String {
        let data = self.data(using: String.Encoding.utf8)!
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
}

extension String {
    func removingWhitespaces(_ str : String!) -> String {
        
        let trimmed = str.trimmingCharacters(in: .whitespacesAndNewlines)
    
        return trimmed
    }
}

extension UserDefaults {
    func contains(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}


extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return String(self[(start ..< end)])
    }
    
    
}

extension UIView
{
    func applyShadow()
    {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 1
       
    }
}

