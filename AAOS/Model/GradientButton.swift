//
//  GradientButton.swift
//  VeriDocG
//
//  Created by Bhavdip Patel on 18/08/18.
//  Copyright © 2018 SevenBits. All rights reserved.
//

import UIKit


    
@IBDesignable
class GradientButton: UIButton {
        let gradientLayer = CAGradientLayer()
        
        @IBInspectable
        var topGradientColor: UIColor? {
            didSet {
                setGradient(topGradientColor: topGradientColor, middleGradientColor:middleGradientColor, bottomGradientColor: bottomGradientColor)
            }
        }
    
        @IBInspectable
        var middleGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: topGradientColor, middleGradientColor:middleGradientColor, bottomGradientColor: bottomGradientColor)
         }
        }
        
        @IBInspectable
        var bottomGradientColor: UIColor? {
            didSet {
                setGradient(topGradientColor: topGradientColor, middleGradientColor:middleGradientColor, bottomGradientColor: bottomGradientColor)
            }
        }
    
    
    
        
    private func setGradient(topGradientColor: UIColor?, middleGradientColor: UIColor?, bottomGradientColor: UIColor?) {
        
            if let topGradientColor = topGradientColor, let middleGradientColor = middleGradientColor, let bottomGradientColor = bottomGradientColor {
                gradientLayer.frame = bounds
                gradientLayer.colors = [topGradientColor.cgColor, middleGradientColor.cgColor, bottomGradientColor.cgColor]
                gradientLayer.borderColor = layer.borderColor
                gradientLayer.borderWidth = layer.borderWidth
                gradientLayer.cornerRadius = layer.cornerRadius
                layer.insertSublayer(gradientLayer, at: 0)
                
            } else {
                gradientLayer.removeFromSuperlayer()
            }
        }
    }


