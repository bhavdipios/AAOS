
//  VeriDocG
//
//  Created by Bhavdip Patel on 05/07/18.
//  Copyright © 2018 SevenBits. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import PKHUD

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class Webservices_Alamofier: NSObject {

    //POST Method
    
    class func postWithURLWithoutHeader(serverlink:String, methodname:String, param:NSDictionary, key:String,  CompletionHandler : @escaping  (Bool,NSDictionary) -> ())
    {
        if Connectivity.isConnectedToInternet {
            
            print("Yes! internet is available.")
            
            //  print("POST : " + serverlink + methodname + " and Param \(param) ")
            
            var fullLink = serverlink
            
            if fullLink.characters.count > 0 {
                
                fullLink = serverlink + methodname
            }
            else {
                
                fullLink = methodname
            }
            
            var configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 30 // seconds
            configuration.timeoutIntervalForResource = 30
            
            
            // create a session manager with the configuration
            //     let sessionManager = Alamofire.SessionManager(configuration: configuration)
            
            // get the default headers
            var headers = Alamofire.SessionManager.defaultHTTPHeaders
            // add your custom header
            headers["contentType"] = "application/json"
            
            // create a custom session configuration
            configuration = URLSessionConfiguration.default
            // add the headers
            configuration.httpAdditionalHeaders = headers
           // .validate(contentType: ["application/json"])
            // create a session manager with the configuration
            Alamofire.SessionManager(configuration: configuration)
            
            print("API URL :",fullLink)
            print("Request Parameters :",param)
            
            
            //HUD.show(.progress)
           // HUD.show(HUDContentType.rotatingImage(UIImage(named: "icn_spinner")))
            
            Alamofire.request(fullLink, method: .post, parameters: param as? Parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response) in
                
                //print(response)
                
                PKHUD.sharedHUD.hide()
                
                if let TempresponseDict:NSDictionary = response.result.value as? NSDictionary {
                    
                    
                    print("response of \(fullLink)")
                    print(TempresponseDict)
                    
                    let returncode = TempresponseDict["returncode"] as! NSInteger
                    
                    if returncode == 1 {
                        
                        CompletionHandler(true, TempresponseDict)
                        
                    }else if returncode == 8 {
                        
                        Toast(text: (param["email"] as! String?)! + " is already a Veri doc account. Try another email or sign in now").show()
                        
                    }else {
                        
                        let myCar = ConstantsModel()
                        let str_Error = myCar.gettingErrorMsg(reposneData: TempresponseDict) as String
                        
                        print(str_Error)
                        
                        if str_Error == "unknown" {
                            
                            let error = response.result.error as? AFError
                            
                            let str_ErrorServer = error?.localizedDescription
                            Toast(text: str_ErrorServer).show()
                        }else {
                            Toast(text: str_Error).show()
                        }
                        
                        CompletionHandler(false, TempresponseDict)
                        
                    }
                }
                
            }
            
            
        }else{
            Toast(text: ConstantsModel.AlertMessage.NetworkConnection).show()
        }
        
    }
    
    
    class func postWithURL(serverlink:String, methodname:String, param:NSDictionary, key:String,  CompletionHandler : @escaping  (Bool,NSDictionary) -> ())
    {
        if Connectivity.isConnectedToInternet {
            
            print("Yes! internet is available.")
            
          //  print("POST : " + serverlink + methodname + " and Param \(param) ")
            
            var fullLink = serverlink
            
            if fullLink.characters.count > 0 {
                
                fullLink = serverlink + methodname
            }
            else {
                
                fullLink = methodname
            }
            
            var configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 30 // seconds
            configuration.timeoutIntervalForResource = 30
           
            
            // create a session manager with the configuration
       //     let sessionManager = Alamofire.SessionManager(configuration: configuration)
            
            // get the default headers
            var headers = Alamofire.SessionManager.defaultHTTPHeaders
            // add your custom header
            headers["apikey"] = ConstantsModel.WebServiceUrl.APIKEY
            print(ConstantsModel.WebServiceUrl.APIKEY)
            // create a custom session configuration
            configuration = URLSessionConfiguration.default
            // add the headers
            configuration.httpAdditionalHeaders = headers
            
            // create a session manager with the configuration
             Alamofire.SessionManager(configuration: configuration)
            
            print("API URL :",fullLink)
            print("Request Parameters :",param)
            
           
            //HUD.show(.progress)
           // HUD.show(HUDContentType.rotatingImage(UIImage(named: "icn_spinner")))
    
            Alamofire.request(fullLink, method: .post, parameters: param as? Parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response) in
                
               print(response)
            
                PKHUD.sharedHUD.hide()
                
                if let TempresponseDict:NSDictionary = response.result.value as? NSDictionary {
                    
                    
                    print("response of \(fullLink)")
                    print(TempresponseDict)
                    
                    let returncode = TempresponseDict["returncode"] as! NSInteger
                    
                    if returncode == 1 {
                        
                         CompletionHandler(true, TempresponseDict)
                        
                    }else if returncode == 8 {
                        
                        CompletionHandler(true, TempresponseDict)
                      
                        
                    }else {
                     
                        let myCar = ConstantsModel()
                        let str_Error = myCar.gettingErrorMsg(reposneData: TempresponseDict) as String
                        
                        print(str_Error)
                       
                        if str_Error == "unknown" {
                            
                            let error = response.result.error as? AFError
                            
                            let str_ErrorServer = error?.localizedDescription
                            Toast(text: str_ErrorServer).show()
                        }else {
                            Toast(text: str_Error).show()
                        }
                        
                         CompletionHandler(false, TempresponseDict)
                   
                  }
             }
    
        }
       
            
        }else{
            Toast(text: ConstantsModel.AlertMessage.NetworkConnection).show()
        }

    }
    
    class func LoginStatus(serverlink:String, methodname:String, param:NSDictionary, key:String,  CompletionHandler : @escaping  (Bool,NSDictionary) -> ())
    {
        if Connectivity.isConnectedToInternet {
            
            print("Yes! internet is available.")
            
            //  print("POST : " + serverlink + methodname + " and Param \(param) ")
            
            var fullLink = serverlink
            
            if fullLink.characters.count > 0 {
                
                fullLink = serverlink + methodname
            }
            else {
                
                fullLink = methodname
            }
            
            var configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 30 // seconds
            configuration.timeoutIntervalForResource = 30
            
            
            // create a session manager with the configuration
            //     let sessionManager = Alamofire.SessionManager(configuration: configuration)
            
            // get the default headers
            var headers = Alamofire.SessionManager.defaultHTTPHeaders
            // add your custom header
            headers["apikey"] = ConstantsModel.WebServiceUrl.APIKEY
            
            // create a custom session configuration
            configuration = URLSessionConfiguration.default
            // add the headers
            configuration.httpAdditionalHeaders = headers
            
            // create a session manager with the configuration
            Alamofire.SessionManager(configuration: configuration)
            
            print("API URL :",fullLink)
            print("Request Parameters :",param)
            
            
            //HUD.show(.progress)
           // HUD.show(HUDContentType.rotatingImage(UIImage(named: "icn_spinner")))
            
            Alamofire.request(fullLink, method: .post, parameters: param as? Parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response) in
                
                //print(response)
                
                //PKHUD.sharedHUD.hide()
                
                if let TempresponseDict:NSDictionary = response.result.value as? NSDictionary {
                    
                    
                    print("response of \(fullLink)")
                    print(TempresponseDict)
                    
                    let returncode = TempresponseDict["returncode"] as! NSInteger
                    
                    if returncode > 0 {
                        
                        CompletionHandler(true, TempresponseDict)
                        
                    }else {
                        
                        let myCar = ConstantsModel()
                        let str_Error = myCar.gettingErrorMsg(reposneData: TempresponseDict) as String
                        
                        print(str_Error)
                        
                        if str_Error == "unknown" {
                            
                            let error = response.result.error as? AFError
                            
                            let str_ErrorServer = error?.localizedDescription
                            Toast(text: str_ErrorServer).show()
                        }else {
                            Toast(text: str_Error).show()
                        }
                        
                        CompletionHandler(false, TempresponseDict)
                        
                    }
                }
                
            }
            
            
        }else{
            Toast(text: ConstantsModel.AlertMessage.NetworkConnection).show()
        }
        
    }
    
    
    //GET Method
    
    class func getWithURL(_ serverlink:String, methodname:String, param:NSDictionary, CompletionHandler:@escaping (Bool,NSDictionary) -> ())
    {
        
        
        if Connectivity.isConnectedToInternet {
            print("Yes! internet is available.")
            // do some tasks..
            print("GET: " + serverlink + "/" + methodname + " and Param : \(param)")
            
            var fullLink = serverlink
            
            if fullLink.characters.count > 0 {
                
                fullLink = serverlink + "/" + methodname
            }
            else {
                
                fullLink = methodname
            }
            
            
            
            
            Alamofire.request(fullLink, method: .get, parameters: param as? Parameters, encoding: URLEncoding.httpBody, headers: [:]).responseJSON { (response) in
                
                print(response)
                
                if let TempresponseDict:NSDictionary = response.result.value as? NSDictionary {
                    
                    
                    print("response of \(fullLink)")
                    print(TempresponseDict)
                    
                    if (TempresponseDict.object(forKey: "response") as? String)?.caseInsensitiveCompare("success") == .orderedSame {
                        
                        CompletionHandler(true, TempresponseDict)
                    }
                    else {
                        
                        var statusCode = response.response?.statusCode
                        
                        if let error = response.result.error as? AFError {
                            
                            statusCode = error._code // statusCode private
                            
                            switch error {
                                
                            case .invalidURL(let url):
                                print("Invalid URL: \(url) - \(error.localizedDescription)")
                            case .parameterEncodingFailed(let reason):
                                print("Parameter encoding failed: \(error.localizedDescription)")
                                print("Failure Reason: \(reason)")
                            case .multipartEncodingFailed(let reason):
                                print("Multipart encoding failed: \(error.localizedDescription)")
                                print("Failure Reason: \(reason)")
                            case .responseValidationFailed(let reason):
                                print("Response validation failed: \(error.localizedDescription)")
                                print("Failure Reason: \(reason)")
                                
                                switch reason {
                                    
                                case .dataFileNil, .dataFileReadFailed:
                                    print("Downloaded file could not be read")
                                case .missingContentType(let acceptableContentTypes):
                                    print("Content Type Missing: \(acceptableContentTypes)")
                                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                                    print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                                case .unacceptableStatusCode(let code):
                                    print("Response status code was unacceptable: \(code)")
                                    statusCode = code
                                }
                            case .responseSerializationFailed(let reason):
                                
                                print("Response serialization failed: \(error.localizedDescription)")
                                print("Failure Reason: \(reason)")
                                // statusCode = 3840 ???? maybe..
                            }
                            
                            print("Underlying error: \(String(describing: error.underlyingError))")
                        }
                        else if let error = response.result.error as? URLError {
                            
                            print("URLError occurred: \(error)")
                        }
                        else {
                            
                            print("Unknown error: \(String(describing: response.result.error))")
                        }
                        
                        print("\(String(describing: statusCode))") // the status code
                        
                        CompletionHandler(false, TempresponseDict)
                    }
                    //                }
                    //                else {
                    //                    //print("Fail \(fullLink)")
                    //                    CompletionHandler(false, NSDictionary())
                    //                }
                }
            }
        }
        else{
            print("internet not connect")
        }
        
    }
    
    class func postWithURLVerify(serverlink:String, methodname:String, param:NSDictionary, key:String,  CompletionHandler : @escaping  (Bool,NSDictionary) -> ())
    {
        if Connectivity.isConnectedToInternet {
            
            print("Yes! internet is available.")
            
            //  print("POST : " + serverlink + methodname + " and Param \(param) ")
            
            var fullLink = serverlink
            
            if fullLink.characters.count > 0 {
                
                fullLink = serverlink + methodname
            }
            else {
                
                fullLink = methodname
            }
            
            var configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 30 // seconds
            configuration.timeoutIntervalForResource = 30
            
            
            // create a session manager with the configuration
            //     let sessionManager = Alamofire.SessionManager(configuration: configuration)
            
            // get the default headers
            var headers = Alamofire.SessionManager.defaultHTTPHeaders
            // add your custom header
            headers["apikey"] = ConstantsModel.WebServiceUrl.APIKEY
            
            // create a custom session configuration
            configuration = URLSessionConfiguration.default
            // add the headers
            configuration.httpAdditionalHeaders = headers
            
            // create a session manager with the configuration
            Alamofire.SessionManager(configuration: configuration)
            
            print("API URL :",fullLink)
            print("Request Parameters :",param)
            
            
            //HUD.show(.progress)
          //  HUD.show(HUDContentType.rotatingImage(UIImage(named: "icn_spinner")))
            
            Alamofire.request(fullLink, method: .post, parameters: param as? Parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response) in
                
                print(response)
                
                PKHUD.sharedHUD.hide()
                
                if let TempresponseDict:NSDictionary = response.result.value as? NSDictionary {
                    
                    
                    
                    print("response of \(fullLink)")
                    print(TempresponseDict)
                    
                    let returncode = TempresponseDict["returncode"] as! NSInteger
                    
                    
                    
                    if returncode > 0 {
                        if returncode == 8 {
                            
                            Toast(text: (param["email"] as! String?)! + " is already a Veridoc Global account. Try another email or sign in now").show()
                            
                        }else {
                            CompletionHandler(true, TempresponseDict)
                        }
                        
                    }else {
                        
                        let myCar = ConstantsModel()
                        let str_Error = myCar.gettingErrorMsg(reposneData: TempresponseDict) as String
                        
                        print(str_Error)
                        
                        if str_Error == "unknown" {
                            
                            let error = response.result.error as? AFError
                            
                            let str_ErrorServer = error?.localizedDescription
                            Toast(text: str_ErrorServer).show()
                        }else {
                            Toast(text: str_Error).show()
                        }
                        
                        CompletionHandler(false, TempresponseDict)
                        
                    }
                }
                
            }
            
            
        }else{
            Toast(text: ConstantsModel.AlertMessage.NetworkConnection).show()
        }
        
    }
    
    class func sendPushNotiWithURL(_ serverlink:String, methodname:String, param:NSDictionary, CompletionHandler:@escaping (Bool,NSDictionary) -> ())
    {
        
        
        if Connectivity.isConnectedToInternet {
            
            
            print("Yes! internet is available.")
            // do some tasks..
            print("GET: " + serverlink + "/" + methodname + " and Param : \(param)")
            
            var fullLink = serverlink
            
            if fullLink.characters.count > 0 {
                
                fullLink = serverlink + "/" + methodname
            }
            else {
                
                fullLink = methodname
            }
            
            
            
            
            Alamofire.request(fullLink, method: .get, parameters: param as? Parameters, encoding: URLEncoding.httpBody, headers: [:]).responseJSON { (response) in
                
                print(response)
                
                if let TempresponseDict:NSDictionary = response.result.value as? NSDictionary {
                    
                    
                    print("response of \(fullLink)")
                    print(TempresponseDict)
                    
                    if (TempresponseDict.object(forKey: "response") as? String)?.caseInsensitiveCompare("success") == .orderedSame {
                        
                        CompletionHandler(true, TempresponseDict)
                    }
                    else {
                        
                        var statusCode = response.response?.statusCode
                        
                        if let error = response.result.error as? AFError {
                            
                            statusCode = error._code // statusCode private
                            
                            switch error {
                                
                            case .invalidURL(let url):
                                print("Invalid URL: \(url) - \(error.localizedDescription)")
                            case .parameterEncodingFailed(let reason):
                                print("Parameter encoding failed: \(error.localizedDescription)")
                                print("Failure Reason: \(reason)")
                            case .multipartEncodingFailed(let reason):
                                print("Multipart encoding failed: \(error.localizedDescription)")
                                print("Failure Reason: \(reason)")
                            case .responseValidationFailed(let reason):
                                print("Response validation failed: \(error.localizedDescription)")
                                print("Failure Reason: \(reason)")
                                
                                switch reason {
                                    
                                case .dataFileNil, .dataFileReadFailed:
                                    print("Downloaded file could not be read")
                                case .missingContentType(let acceptableContentTypes):
                                    print("Content Type Missing: \(acceptableContentTypes)")
                                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                                    print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                                case .unacceptableStatusCode(let code):
                                    print("Response status code was unacceptable: \(code)")
                                    statusCode = code
                                }
                            case .responseSerializationFailed(let reason):
                                
                                print("Response serialization failed: \(error.localizedDescription)")
                                print("Failure Reason: \(reason)")
                                // statusCode = 3840 ???? maybe..
                            }
                            
                            print("Underlying error: \(String(describing: error.underlyingError))")
                        }
                        else if let error = response.result.error as? URLError {
                            
                            print("URLError occurred: \(error)")
                        }
                        else {
                            
                            print("Unknown error: \(String(describing: response.result.error))")
                        }
                        
                        print("\(String(describing: statusCode))") // the status code
                        
                        CompletionHandler(false, TempresponseDict)
                    }
                    //                }
                    //                else {
                    //                    //print("Fail \(fullLink)")
                    //                    CompletionHandler(false, NSDictionary())
                    //                }
                }
            }
        }
        else{
            print("internet not connect")
        }
        
    }
    //Send Image
    
    class func postImageToUrl(_ serverlink:String,methodname:String,param:NSDictionary,image:UIImage!,withImageName : String,CompletionHandler:@escaping (Bool,NSDictionary) -> ()) {
        
        if Connectivity.isConnectedToInternet {
            print("Yes! internet is available.")
        
            print("POST : " + serverlink + methodname + " and Param \(param) ")
        
            var fullLink = serverlink
            
            if fullLink.characters.count > 0 {
                
                fullLink = serverlink + "/" + methodname
            }
            else {
                
                fullLink = methodname
            }
            
            var imgData = Data()
            if image != nil {
                
                imgData = UIImageJPEGRepresentation(image!, 1.0)!
            }
            
            let notallowchar : CharacterSet = CharacterSet(charactersIn: "01234").inverted
            let dateStr:String = "\(Date())"
            let resultStr:String = (dateStr.components(separatedBy: notallowchar) as NSArray).componentsJoined(by: "")
            let imagefilename = resultStr + ".jpg"
            
            Alamofire.upload(multipartFormData:{ multipartFormData in
                multipartFormData.append(imgData, withName: withImageName as String, fileName: imagefilename, mimeType: "image/jpeg")
                
                for (key, value) in param {
                    
                    //let data = (value as! String).data(using: String.Encoding.utf8)!
                    let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue)
                    multipartFormData.append(data!, withName: key as! String)
                    
                    
                    //                if value is [String]{
                    //                    let data = value.data(using: String.Encoding.utf8.rawValue)
                    //                    multipartFormData.append(data!, withName: key)
                    //                }
                    //                else if value is String{
                    //                    let data = value.data(using: String.Encoding.utf8.rawValue)
                    //                    multipartFormData.append(data!, withName: key)
                    //                }
                    //                else if let v = value as? Bool{
                    //                    var bValue = v
                    //                    let d = bValue.dataUsingEncoding(String.Encoding.utf8)! //NSData.init(bytes: &bValue, length: MemoryLayout<Bool>.size) //NSData(bytes: &bValue, length: MemoryLayout<Bool>.size)
                    //                    multipartFormData.append(d, withName: key)
                    //                    multipartFormData. .append(d, withName: key)
                    //                }
                }
            },
            usingThreshold:UInt64.init(),
            to:fullLink,
            method:.post,
            headers:[:],
            encodingCompletion: { encodingResult in
            
                switch encodingResult {
                                    
                    case .success(let upload, _, _):
                                    
                    upload.uploadProgress { progress in // main queue by default
                                     
                        print("Upload Progress: \(progress.fractionCompleted)")
                    }
                                    
                    upload.responseJSON { response in
                        
                        print(response)
                        
                                        
                        if let TempresponseDict:NSDictionary = response.result.value as? NSDictionary {
                                            
                            if (TempresponseDict.object(forKey: "response") as? String)?.caseInsensitiveCompare("success") == .orderedSame {
                                                
                                CompletionHandler(true, TempresponseDict)
                            }
                            else {
                                
                                var statusCode = response.response?.statusCode
                                
                                if let error = response.result.error as? AFError {
                                    
                                    statusCode = error._code // statusCode private
                                    
                                    switch error {
                                        
                                    case .invalidURL(let url):
                                        print("Invalid URL: \(url) - \(error.localizedDescription)")
                                    case .parameterEncodingFailed(let reason):
                                        print("Parameter encoding failed: \(error.localizedDescription)")
                                        print("Failure Reason: \(reason)")
                                    case .multipartEncodingFailed(let reason):
                                        print("Multipart encoding failed: \(error.localizedDescription)")
                                        print("Failure Reason: \(reason)")
                                    case .responseValidationFailed(let reason):
                                        print("Response validation failed: \(error.localizedDescription)")
                                        print("Failure Reason: \(reason)")
                                        
                                        switch reason {
                                            
                                        case .dataFileNil, .dataFileReadFailed:
                                            print("Downloaded file could not be read")
                                        case .missingContentType(let acceptableContentTypes):
                                            print("Content Type Missing: \(acceptableContentTypes)")
                                        case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                                            print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                                        case .unacceptableStatusCode(let code):
                                            print("Response status code was unacceptable: \(code)")
                                            statusCode = code
                                        }
                                    case .responseSerializationFailed(let reason):
                                        
                                        print("Response serialization failed: \(error.localizedDescription)")
                                        print("Failure Reason: \(reason)")
                                        // statusCode = 3840 ???? maybe..
                                    }
                                    
                                    print("Underlying error: \(String(describing: error.underlyingError))")
                                }
                                else if let error = response.result.error as? URLError {
                                    
                                    print("URLError occurred: \(error)")
                                }
                                else {
                                    
                                    print("Unknown error: \(String(describing: response.result.error))")
                                }
                                
                                print("\(String(describing: statusCode))") // the status code
                                
                                
                                CompletionHandler(false, TempresponseDict)
                            }
                        }
                        else {
                                            
                            CompletionHandler(false, NSDictionary())
                        }
                    }
                    
                    case .failure(let encodingError):
                        
                        print(encodingError)
                        
                        CompletionHandler(false, NSDictionary())
                }
            })
        }
        else{
             print("internet not connect")
        }
    
    }
}

