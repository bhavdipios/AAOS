
//  VeriDocG
//
//  Created by Bhavdip Patel on 05/07/18.
//  Copyright © 2018 SevenBits. All rights reserved.
//
import UIKit

struct ConstantsModel {
    
    
    //Base Url Path :-
    
    struct BasePath {
        // Live Link
       // static let MAINLINK  = "https://veridocglobal.com/Api/"
        //static let SmartLoginLink = "https://veridocglobal.com/login"
        
        
         static let MAINLINK  = "https://mystage.veridocglobal.com/Api/"
         static let SmartLoginLink = "https://mystage.veridocglobal.com/login"
    }
    
    //MARK: - device type
    enum UIUserInterfaceIdiom : Int{
        case Unspecified
        case Phone
        case Pad
    }
    
//    struct ScreenSize {
//        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
//        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
//        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
//        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
//    }
//
//    struct DeviceType
//    {
//        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
//        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
//        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
//        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
//        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
//    }
//
    // MARK: - Global Utility
    struct GlobalConstants {
        static let appName    = Bundle.main.infoDictionary!["CFBundleName"] as! String
        static let iPhoneStoryboard = UIStoryboard(name: "Main", bundle: nil)
        static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    }
    
    // MARK: - WebService URL
    // local : ""
    // live : ""
    struct WebServiceUrl {
        
        static let url_ForgotPassword = "https://veridocglobal.com/passwordrecovery"
        static let APIKEY = "ED0CE75F4407B1AA7A25197AE244E1D4884E67D598C59E0F9DCDF379386179B0"
        static let mainURL = "https://veridocglobal.com/Api/"
            //static let mainURL = "https://mystage.veridocglobal.com/Api/"
            // static let mainURL = "https://livepreviewlogin.veridocglobal.com/API/"
        static let API_Register = mainURL + "Register"
        static let API_Login = mainURL + "CheckLogin"
        static let API_contactus = mainURL + "contactus"
        static let API_changepassword = mainURL + "changepassword"
        static let API_passwordrecovery = mainURL + "passwordrecovery"
        static let ContactUsURL = mainURL + ""
        static let API_IsEmailValidate = mainURL + "IsEmailValidate"
        static let API_ResendEmail = mainURL + "ResendEmail"
        static let API_UpdateEmail = mainURL + "UpdateEmail"
        static let API_Applogout = mainURL + "applogout"
        static let API_IsWebLogin = mainURL + "IsWebLogin"
        static let API_updatecustomer = mainURL + "updatecustomer"
    }

    
    // MARK: - StoryBoard Identifier's
    
    struct StoryBoardID{
        
        static let kStructureScreenVC = "StructureScreenVC"
        static let kStructureDetailScreenVC = "StructureDetailScreenVC"
        static let kStructureSubDetailScreenVC = "StructureSubDetailScreenVC"
        static let kRouteOnMapViewScreenVC = "RouteOnMapViewScreenVC"
        static let kArchitectureViewScreenVC = "ArchitectureViewScreenVC"
        static let kAboutUsScreenVC = "AboutUsScreenVC"
        static let kAboutDetailViewScreenVC = "AboutDetailViewScreenVC"
        static let kArchitectureDetailScreenVC = "ArchitectureDetailScreenVC"
        static let kTourMapViewScreenVC = "TourMapViewScreenVC"
        static let kSearchViewScreenVC = "SearchViewScreenVC"
        static let kFavoriteViewScreenVC = "FavoriteViewScreenVC"
        
        
        
    }
    
    
    
    // MARK: - Message's
    struct AlertMessage {
        
        static let NetworkConnection  = "You are not connected to internet. Please connect and try again"
        
        //Login And Registration Alert message's
        static let EmptyFirstName = "First name is required"
        static let InValidFirstName = "Enter valid first name"
        static let InvalidFirstNameRange = "Please enter first name between 2 to 20 characters!"
        static let EmptyLastName = "Last name is required"
        static let InValidLastName = "Enter valid last name"
        static let InvalidLastNameRange = "Please enter last name between 2 to 20 characters!"
        static let EmptyEmail = "Email is required"
        static let InValidEmail = "Enter valid email id"
        static let EmptyPassword = "Password is required"
        static let InvalidPassword = "Enter valid password"
        static let InvalidPasswordRange = "Please enter password between 8 To 15 characters!"
        static let EmptyConfirmPassword = "Confirm password is required"
        static let SamePassword = "Enter same password"
        
        //FeedBack Alert message's
        static let heartRate = "Heart rate is required"
        static let BodyTemp = "Body temp is required"
        static let InvalidHeartRate = "Enter valid heart rate"
        static let InvalidHeartRateRange = "Please enter heart rate between 10 To 300"
        static let InvalidBodyTemp = "Enter valid body temp"
        static let InvalidBodyTempRange = "Please enter body temp between 90 To 110"
        
        //Contact Us Alert message's
        static let EmptyName = "Name is required"
        static let InValidName = "Enter valid name"
        static let InvalidNameRange = "Please enter name between 2 To 40 characters!"
        static let EmptyPhoneNumber = "Phone number is required"
        static let InvalidPhoneNumber = "Enter valid phone number"
        static let EmptyComment = "Comment is required"
        static let InvalidCommentRange = "Please enter comment minimum 20 characters!"
    }
    
    // MARK: - ApiConstants
    struct ApiConstants {
        static let paramEmail = "email_id"
        static let ParamuserID = "user_id"
        static let paramPassword = "password"
        static let paramFname = "first_name"
        static let paramLname = "last_name"
        static let paramisUserActive = "isUserActive"
        static let isRemember = "isRemember"
        static let password = "password"
    }
    
    // MARK: - NSDefaultUserKeys
    struct KeyDefaultUser {
        
        static let userData = "userdata"
        static let smartlogindetail = "smartlogindetail"
        
    }
    
    // MARK: - HandleError
    
        
      //  static var gettingErrorMsg(reposneData: NSDictionary) -> String
       func gettingErrorMsg(reposneData: NSDictionary) -> String {
        
        let returncode = reposneData["returncode"] as! NSInteger
        
        if returncode == 0 {
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "InvalidParameter"
            }else{
                return (str_serverMsg as String?)!
            }
            
        }else if returncode == 1{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "Success"
            }else{
                return (str_serverMsg as String?)!
            }
            
        }else if returncode == 2{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "UnauthorizedUser"
            }else{
                return (str_serverMsg as String?)!
            }
            
            
            
        }else if returncode == 3{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "InvalidEmail"
            }else{
                return (str_serverMsg as String?)!
            }
            
            
        }else if returncode == 4{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "InternalServerError"
            }else{
                return (str_serverMsg as String?)!
            }
           
            
        }else if returncode == 5{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "RecordNotFound"
            }else{
                return (str_serverMsg as String?)!
            }
            
            
            
        }else if returncode == 6{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "Verified"
            }else{
                return (str_serverMsg as String?)!
            }
           
            
        }else if returncode == 7{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "NotVerified"
            }else{
                return (str_serverMsg as String?)!
            }
          
            
        }else if returncode == 8{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "Duplicate"
            }else{
                return (str_serverMsg as String?)!
            }
           
            
        }else if returncode == 10{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "Failed"
            }else{
                return (str_serverMsg as String?)!
            }
           
            
        }else if returncode == 20{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "InProgress"
            }else{
                return (str_serverMsg as String?)!
            }
           
            
        }else if returncode == 30{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "Complete"
            }else{
                return (str_serverMsg as String?)!
            }
           
            
        }else if returncode == 11{
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "InvalidUsername"
            }else{
                return (str_serverMsg as String?)!
            }
          
        }else if returncode == 12{
            
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "InvalidPassword"
            }else{
                return (str_serverMsg as String?)!
            }
           
        }else if returncode == 13{
            let str_serverMsg = reposneData["returnmessage"] as? String
            
            if str_serverMsg == nil{
                return "NotAbleToLogin"
            }else{
                return (str_serverMsg as String?)!
            }
           // return ""
        }
        else{
            return "unknown"
        }
       
      }
    
    
    struct ColorCode {
        static let kcolor_gray : UIColor = UIColor(red: 245.0/255.0, green: 246.0/255.0, blue: 246.0/255.0, alpha: 1.0)
        static let kColor_Theme: UIColor = UIColor(red: 38.0/255.0, green: 150.0/255.0, blue: 77.0/255.0, alpha: 1.0)
    }
    
    struct URL_SocialMediaFollow {
       
         static let str_fb  = "https://www.facebook.com/VeriDocGlobal"
         static let  str_AppFb  = "fb://www.facebook.com/VeriDocGlobal"
         static let str_telegram  = "http://t.me/VeriDocGlobal"
         static let str_twitter  = "https://twitter.com/VeriDocGlobal"
         static let str_instagram  = "https://www.instagram.com/VeriDocGlobal/"
         static let str_linkedln  = "https://www.linkedin.com/company/veridocglobal"
         static let str_youtube  = "https://www.youtube.com/channel/UCbl5uvM3vd-XRm-aDj2YZJw"
       
    }
}


