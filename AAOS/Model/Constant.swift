
//  VeriDocG
//
//  Created by Bhavdip Patel on 05/07/18.
//  Copyright © 2018 SevenBits. All rights reserved.
//


import UIKit


class Constant: NSObject {
    

    static var L_1_DailyLimit = ""
    
    
    
    struct GlobalConstants {
        
        
        static let str_Site = "https://veridocglobal.com"
        static let str_Call = "0434 284 295"
        static let str_Mail = "admin@veridocglobal.com"
        
        // Mail Constant
        static let str_M_username = "0a4560cc2681cf4af49f508fcae03e49"
        static let str_M_Password = "95f786c40f422556ec78e978b4b583ef"
        static let str_M_From = "admin@veridocglobal.com"
        static let str_M_Display = "VeriDoc Global"
        static let str_M_mailhost = "in-v3.mailjet.com"
        static let str_M_port = 465
        
        
        static let str_ScratchforFirstStep = 18
        static let str_tokenforFirstStep = 1500
        
        static let str_ScratchforSecondStep = 36
        static let str_tokenforSecondStep = 1500
        
        static let str_ScratchforThirdStep = 60
        static let str_tokenforThirdStep = 2000
        
        
        
        static let str_TotalScratchesPerDay = 3
        static let str_TotalScratchesPerLevel = 18
        static let str_RemainingScratch = 18
        static let str_TotalScratchesPerWin = 3
        
        static let str_TotalScratchesPerInvited = 5
        static let str_TotalScratchesPerShared = 5
        static let str_ScratchesPerBlogShared = 5
        static let str_ScratchesPerTestimonialShared = 5
        
        static let str_TotalTokenPerWin = 1000
        static let str_TotalTokenPerLevel = 5000
        static let str_TotalTokenPerRedeem = 15000
        static let str_BonusToken_FinishFirstLevel = 5000
        
        
        static let str_scratch_FirstLevel = 60
        static let str_scratch_SecondLevel = 60
        
        static let TotalPrizePerRedeemBox = 5

        static let str_FCMserverKey = "AAAALaM9MfE:APA91bHHlEWoKb5YDJTKbXOFk0Bw0OYbMK7X0qLlLrSiHhe7oZmoBfJ84JIAELafbMqJcZGP1XXDUTjfGQxKhW1o0kvTwwOFewPfXqE3kpeZ7PQ-1mv18ehfc0hUfBme6t2ZYtFoW11S"
       
        static let kColor_TextTheme: UIColor = UIColor.white
        
      //  static let kColor_Theme: UIColor = UIColor(red: 96.0/255.0, green: 175.0/255.0, blue: 70.0/255.0, alpha: 1.0)
          static let kColor_Theme: UIColor = UIColor(red: 38.0/255.0, green: 150.0/255.0, blue: 77.0/255.0, alpha: 1.0)
       // static let kColor_Theme: UIColor = UIColor.red
        
        
        static let kShowViewBK: UIColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        
        static let kColor_Compliant: UIColor = UIColor(red: 87.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0)

        
        // Check Device IPHONE
        
        static let kIphone_4s : Bool =  (UIScreen.main.bounds.size.height == 480)

        static let kIphone_5 : Bool =  (UIScreen.main.bounds.size.height == 568)
 
        static let kIphone_6 : Bool =  (UIScreen.main.bounds.size.height == 667)

        static let kIphone_6_Plus : Bool =  (UIScreen.main.bounds.size.height == 736)
        
        // Response searchList
        static let themeFontNormal                =    "Helvetica Neue"
        static let themeFontBold                  =    "HelveticaNeue-Bold"
        static let kBirthDate                     =    "DateOfBirth"
        static let kFirstName                     =    "FirstName"
        static let kLastName                      =    "LastName"
        static let kOrganizationId                =    "OrganisationId"
        static let kIndividualID                  =    "IndividualId"
        static let kOrganizationName              =    "OrganisationName"
        static let kText                          =    "text"
        static let kID                            =    "Id"
        static let kOrganizationFullName          =    "FullName"
        
        
        
        
        // Parameters Name
        static let keyfirstname                =    "firstname"
        static let keylastname                =    "lastname"
        static let keyemail                =    "email"
        static let keymobile               =    "mobile"
        static let keyotp                =    "otp"
        static let keypassword                =    "password"
        static let keyuid               =    "uid"
        static let keyprofileImageUrl     =   "profileImageUrl"
        
        
        
        
        //  API Name
        
        static let api_users                =    "users"
    
        
        static let noti_EULAPending = "EULAPending"
        static let noti_Id_Login = "noti_login"
        static let noti_Id_logout = "noti_logout"
        static let noti_decline = "noti_decline"
        
    }
    
     static func GradientImage() -> UIImage {
        
        let color1 = UIColor(red: 37.0/255.0, green: 152.0/255.0, blue: 77.0/255.0, alpha: 1.0)
        let color2 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        let color3 = UIColor(red: 94.0/255.0, green: 177.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        let gradientLayer = CAGradientLayer()
        // gradientLayer.frame =  UINavigationBar.appearance().bounds
        gradientLayer.colors = [color3.cgColor, color2.cgColor, color1.cgColor]
        
        let sizeLength = UIScreen.main.bounds.size.height * 2
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 99)
        gradientLayer.frame = defaultNavigationBarFrame
        // btn_SignIn.setBackgroundImage(self.image(fromLayer: gradientLayer), for: .default)
        
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage!
        
    }
    
    
    
}
