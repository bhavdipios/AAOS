
//  VeriDocG
//
//  Created by Bhavdip Patel on 05/07/18.
//  Copyright © 2018 SevenBits. All rights reserved.
//

import Foundation
import UIKit
let sideMenuVC = KSideMenuVC()
let appDelegate = UIApplication.shared.delegate as! AppDelegate


class kConstant {
    
    
    
   
   
    
   
    
 
    func DetectDevice() -> String {
        
        var str_isIphone = String()
        
        if(UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
            //iPad
            str_isIphone = "Main"
        }
        else {
            //iPhone
            str_isIphone = "Main"
        }
        
        return str_isIphone
    }

    func SetIntialMainViewController(_ aStoryBoardID: String)->(KSideMenuVC){
        
         let mainStoryboard = UIStoryboard(name: DetectDevice(), bundle: nil)
        
        let sideMenuObj = mainStoryboard.instantiateViewController(withIdentifier: "sideMenuID")
        let mainVcObj = mainStoryboard.instantiateViewController(withIdentifier: aStoryBoardID)
        let navigationController : UINavigationController = UINavigationController(rootViewController: mainVcObj)
        navigationController.isNavigationBarHidden = false
        sideMenuVC.view.frame = UIScreen.main.bounds
        sideMenuVC.mainViewController(navigationController)
        sideMenuVC.menuViewController(sideMenuObj)
        return sideMenuVC
        
    }
    func SetMainViewController(_ aStoryBoardID: String)->(KSideMenuVC){
        
         let mainStoryboard = UIStoryboard(name: DetectDevice(), bundle: nil)
        let mainVcObj = mainStoryboard.instantiateViewController(withIdentifier: aStoryBoardID)
        let navigationController : UINavigationController = UINavigationController(rootViewController: mainVcObj)
        navigationController.isNavigationBarHidden = false
        sideMenuVC.view.frame = UIScreen.main.bounds
        sideMenuVC.mainViewController(navigationController)
        return sideMenuVC
    }
    
    
   
   
    // let sideMenuVC = KSideMenuVC()
    
}
