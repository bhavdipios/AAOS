
//
//  SideMenuVC.swift
//  SideMenuSwiftDemo
//
//  Created by Kiran Patel on 1/2/16.
//  Copyright © 2016  SOTSYS175. All rights reserved.
//

import Foundation
import UIKit

import AVFoundation



class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var window: UIWindow?
    
    @IBOutlet var tableView: UITableView!
    
    var arrMenuLogIn = ["Renew Subscription","Home","Notification", "View Top 10 Videos", "Favourite Video", "View By Category", "• Upper Extremity", "• Spine", "• Lower Extremity","• Sport Medicine","• Trauma","Log out"]
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        self.tableView.reloadData()
        self.tableView.backgroundColor=UIColor.clear
        self.view.backgroundColor=UIColor.black
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return arrMenuLogIn.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let aCell = tableView.dequeueReusableCell(
            withIdentifier: "kCell", for: indexPath)
        
        aCell.backgroundColor=UIColor.clear
        
        let lbl_Menu : UILabel = aCell.viewWithTag(2) as! UILabel
        lbl_Menu.textColor = UIColor.white
        lbl_Menu.font = UIFont(name:Constant.GlobalConstants.themeFontNormal, size: 16.0)
        
        lbl_Menu.text = arrMenuLogIn[indexPath.row]
        
        if indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 10 {
            lbl_Menu.font = UIFont.init(name: "Roboto-Thin", size: 18)
            lbl_Menu.textColor = UIColor.lightGray
        }else {
            lbl_Menu.font = UIFont.init(name: "Roboto-Bold", size: 18)
            lbl_Menu.textColor = UIColor.white
        }
        
        
        return aCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        //self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.white]
        
        
        var str_SelectedMenu: String!
        
        str_SelectedMenu = arrMenuLogIn[indexPath.row];
        
        if str_SelectedMenu == "Log out" {
        
            kConstantObj.SetIntialMainViewController("Register_Controller")
        
        }else if str_SelectedMenu == "Renew Subscription" {
            
            kConstantObj.SetIntialMainViewController("Subscription_Controller")
            
        }else {
            
             kConstantObj.SetIntialMainViewController("Home_Controller")
            
        }
            
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 46.0
    }
    
}
