//
//  Subscription_Controller.swift
//  AAOS
//
//  Created by Binoy on 13/03/19.
//  Copyright © 2019 Bhavdip. All rights reserved.
//

import UIKit
import PinCodeTextField

class Subscription_Controller: UIViewController {

    @IBOutlet weak var verificationCodeView: PinCodeTextField!
    @IBOutlet weak var btn_Submit: UIButton!
    @IBOutlet weak var vw_BK_White: UIView!
    var str_OTP: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

      //  self.navigationController?.navigationBar.isHidden = true
        
        verificationCodeView.delegate = self
        verificationCodeView.keyboardType = .numberPad
        
        
        let toolbar = UIToolbar()
        let nextButtonItem = UIBarButtonItem(title: NSLocalizedString("Done",
                                                                      comment: ""),
                                             style: .done,
                                             target: self,
                                             action: #selector(pinCodeNextAction))
        toolbar.items = [nextButtonItem]
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        
        verificationCodeView.inputAccessoryView = toolbar
        
        // Do any additional setup after loading the view.
        
        btn_Submit.layer.cornerRadius = 8
        btn_Submit.clipsToBounds = true
        vw_BK_White.layer.cornerRadius = 8
        vw_BK_White.clipsToBounds = true
        
    }
    
    @IBAction func btn_LeftMenu(_ sender: UIBarButtonItem) {
        
        sideMenuVC.toggleMenu()
        
    }
    
    @objc private func pinCodeNextAction() {
        print("next tapped")
        self.view.endEditing(true)
        
    }

    @IBAction func clearButtonTapped(_ sender: UIButton) {
        verificationCodeView.text = nil
        
    }

}

extension Subscription_Controller: PinCodeTextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: PinCodeTextField) {
        
    }
    
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        let value = textField.text ?? ""
        print("value changed: \(value)")
        str_OTP = value
        
    }
    
    func textFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: PinCodeTextField) -> Bool {
        return true
    }
}




