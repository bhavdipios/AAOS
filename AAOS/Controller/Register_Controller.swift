//
//  Register_Controller.swift
//  AAOS
//
//  Created by Binoy on 11/03/19.
//  Copyright © 2019 Bhavdip. All rights reserved.
//

import UIKit

class Register_Controller: UIViewController {

    @IBOutlet var btn_Submit: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        btn_Submit.layer.cornerRadius = 8
        btn_Submit.clipsToBounds = true
    }
    
    
    @IBAction func btn_Submit(_sender: UIButton) {
        kConstantObj.SetIntialMainViewController("Home_Controller")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
